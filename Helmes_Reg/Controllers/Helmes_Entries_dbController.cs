﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Helmes_Reg;

namespace Helmes_Reg.Controllers
{
    public class Helmes_Entries_dbController : Controller
    {
        private HelmesTestEntities db = new HelmesTestEntities();

        // GET: Helmes_Entries_db
        public ActionResult Index()
        {
            return View(db.Helmes_Entries_db.ToList());
        }

        // GET: Helmes_Entries_db/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Helmes_Entries_db helmes_Entries_db = db.Helmes_Entries_db.Find(id);
            if (helmes_Entries_db == null)
            {
                return HttpNotFound();
            }
            return View(helmes_Entries_db);
        }

        // GET: Helmes_Entries_db/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Helmes_Entries_db/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EntryID,FullName,Sectors,Agree")] Helmes_Entries_db helmes_Entries_db)
        {
            if (ModelState.IsValid)
            {
                db.Helmes_Entries_db.Add(helmes_Entries_db);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(helmes_Entries_db);
        }

      

        // GET: Helmes_Entries_db/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Helmes_Entries_db helmes_Entries_db = db.Helmes_Entries_db.Find(id);
            if (helmes_Entries_db == null)
            {
                return HttpNotFound();
            }
            return View(helmes_Entries_db);
        }

        // POST: Helmes_Entries_db/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EntryID,FullName,Sectors,Agree")] Helmes_Entries_db helmes_Entries_db)
        {
            if (ModelState.IsValid)
            {
                db.Entry(helmes_Entries_db).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(helmes_Entries_db);
        }

        // GET: Helmes_Entries_db/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Helmes_Entries_db helmes_Entries_db = db.Helmes_Entries_db.Find(id);
            if (helmes_Entries_db == null)
            {
                return HttpNotFound();
            }
            return View(helmes_Entries_db);
        }

        // POST: Helmes_Entries_db/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Helmes_Entries_db helmes_Entries_db = db.Helmes_Entries_db.Find(id);
            db.Helmes_Entries_db.Remove(helmes_Entries_db);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
